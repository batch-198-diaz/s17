// index.js
console.log("heyo");

// FUNCTIONS

/*
	Syntax:
		function functionName(){
			code block(statement)
		}
*/

function printName(){
	console.log('My name is Anna');
};

printName(); // invoke (call the function), result: My name is Anna

// declaredFunction(); //result: error, it is not yet defined

// FUNCTION DECLARATION vs. EXPRESSIONS

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction()");
};

declaredFunction();

/*
 Function Expression [function()]
	Anonymous function - function without a name
	and they can be stored in a variable
*/

//variableFunction(); //error (doesn't hoist) - when it comes before the definition

let variableFunction = function(){
	console.log("I am from variableFunction");
};

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side");
};


//You can reassign declared functions and function expression to new anonymous

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

funcExpression = function(){
	console.log("Updated funcExpression");
};

// const still can't be reassigned 

const constantFunction = function(){
	console.log("Initialize const");
};

constantFunction();

/*
constantFunction = function(){
	console.log("Cannot be reassigned!")
};

constantFunction();
*/


// FUNCTION SCOPING

/*
	Javascript Variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/


{
	let localVar = 'local';
}

let globalVar = 'global';

console.log(globalVar);
//console.log(localVar); //error: localVar is not defined

function showNames(){

	//funciton scoped variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

//error, cannot access outside of the function


// NESTED FUNCTION

function myNewFunction(){
	let name = 'first-function-name';

	function nestedFunction(){
		let nestedName = 'second-nested-name';
		console.log(nestedName);
	}
	nestedFunction();

};

myNewFunction();

// Function and Global Scoped Variable

let globalName = 'Alan'; //global scoped variable

function myNewFunction2(){
	let nameInside = 'Marco';
	console.log(globalName); //can access here
};
myNewFunction2(); //result: Alan


// ALERT
/*
	Syntax:
		alert('message');
*/

//alert('Hello World');

function showSampleAlert(){
	alert('Hello User');
};

// showSampleAlert();

console.log("I will only log in the console when the alerts are dismissed.");


// PROMPT
/*
	Syntax:
		prompt("<dialog>");
*/

//let samplePrompt = prompt('Enter your name: ');
//console.log('Hello ' + samplePrompt);

//let sampleNullPrompt = prompt("Click Cancel");
//console.log(sampleNullPrompt);

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name:');
	let lastName = prompt('Enter your last name');

	console.log('Hello ' + firstName +)
};

