// index.js

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printInfo(){
		let full_name = prompt('Enter your full name:');
		let age = prompt('Enter your age:');
		let location = prompt('Enter your location:');

		console.log("Hi " + full_name);
		console.log(full_name + "'s age is " + age);
		console.log(full_name + " is located at " + location);
	};

	printInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printArtists(){
		let fave_artists = ['Stephen Wake','Ichika','Yvette Young','Khruangbin','Motohiro Hata']
		console.log(fave_artists);
	};

	printArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printMovies(){
		let movie_1 = 'Hercules (1997)';
		let movie_2 = 'Tarzan (1999)';
		let movie_3 = 'Pocahontas (1995)';
		let movie_4 = 'Mulan (1998)';
		let movie_5 = 'Lion King (1994)';

		console.log("1. " + movie_1);
		console.log("IMDB Rating for " + movie_1 + ": 7.2 / 10")
		console.log("2. " + movie_2);
		console.log("IMDB Rating for " + movie_2 + ": 7.3 / 10")
		console.log("3. " + movie_3);
		console.log("IMDB Rating for " + movie_3 + ": 6.7 / 10")
		console.log("4. " + movie_4);
		console.log("IMDB Rating for " + movie_4 + ": 7.6 / 10")
		console.log("5. " + movie_5);
		console.log("IMDB Rating for " + movie_5 + ": 8.5 / 10")

	};

	printMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printFriends();

let printFriends = function printUsers(){

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();